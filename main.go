package main

import (
	"fmt"
	"io/ioutil"
	"net/http"
	"os"
	"strconv"
)

type Task struct {
	Description string
	Done        bool
}

type List struct {
	ID   string
	Task string
}

var tasks []Task

func main() {
	http.HandleFunc("/", list)
	http.HandleFunc("/done", done)
	http.HandleFunc("/add", add)

	http.ListenAndServe(":8080", nil)
}

func list(rw http.ResponseWriter, _ *http.Request) {
	liste := []List{}
	task := []Task{
		{"Faire les courses", false},
		{"Payer les factures", false},
	}
	for id, a := range task {
		if !a.Done {
			liste = append(liste, List{strconv.Itoa(id), a.Description})
		}
	}
	rw.WriteHeader(http.StatusOK)

	return
}

func add(rw http.ResponseWriter, r *http.Request) {
	if r.Method != http.MethodPost {
		rw.WriteHeader(http.StatusBadRequest)
		os.Exit(1)
	}
	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		fmt.Printf("Error reading body: %v", err)
		http.Error(
			rw,
			"can't read body", http.StatusBadRequest,
		)
		return
	}
	des := string(body)
	tasks = append(tasks, Task{des, false})
	rw.WriteHeader(http.StatusOK)
}

func done(rw http.ResponseWriter, r *http.Request) {
	switch r.Method {
	case http.MethodGet:

	case http.MethodPost:

	default:
		rw.WriteHeader(http.StatusBadRequest)
		return
	}
}
